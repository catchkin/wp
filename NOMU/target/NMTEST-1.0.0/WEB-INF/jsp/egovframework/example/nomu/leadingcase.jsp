<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>

  <main class="container clearfix">
    <div class="info_box">
      <div class="info_content1">
        <ul>
          <li class="info_tit">노무정보</li>
          <li class="info_sub_tit menuClick"><a href="/leadingcase.do">판례</a></li>
          <li class="info_sub_tit"><a href="/example.do">사례</a></li>
          <li class="info_sub_tit"><a href="/news.do">뉴스</a></li>
        </ul>
      </div>
      
      <div class="info_content2">
        <div class="input_area">
          <input type="text" placeholder="검색어를 입력해주세요">
          <button type="button"><i class="fas fa-search"></i></button>
        </div>
        <div class="table">
          <table class="type01">
            <tr class="table_title">
                <th scope="row">번호</th>
                <td>판례</td>
            </tr>
            <tr class="table_desc">
                <th scope="row">1</th>
                <td>
                  <p>
                    어떠한 임금이 통상임금에 속하는지 여부는 그 임금이 소정근로의 
                    대가로 근로자에게 지급되는 금품으로서 정기적, 일률적, 고정적으로 
                    지급되는 것인지를 기준으로 객관적인 성질에 따라 판단하여야 하고 
                    임금의 명칭이나 지급주기의 장단 등 형식적 기준에 의해 정할 것이 아니다.
                  </p>
                  <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
                </td>
            </tr>
            <tr class="table_desc">
                <th scope="row">2</th>
                <td>
                  <p>
                    *매월 임금지급 시 해당 월의 출근성적에 따라 차등으로 만근수당을
                    지급하고 있다면 이는 소정의 근로에 대하여 일률적으로 지급하는 임금 
                    또는 수당이 아니라 할 것이며, 따라서 최저임금의 적용을 위한 임금에 
                    산입되는 임금으로 보고 어렵다고 사료됨(임금정책과-3207, 2004.08.27)
                  </p>
                  <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
                </td>
            </tr>
            <tr class="table_desc">
              <th scope="row">3</th>
              <td>
                <p>
                  甲 주식회사는 반도체 제조장비 전문 생산업체로서 반도체 소자와 기판 사이에 
                  채워진 부도체를 관통하여 통로를 형성하는 레이저 드릴링 기술을 구현한 반도체 
                  장비의 제조 관련 기술정보를 보유한 회사이고, 乙 등은 甲 회사에서 근무하다가 
                  또 다른 반도체 제조장비 전문 생산업체인 丙 주식회사로 이직한 사람들인데, 
                  甲 회사가 乙 등 및 丙 회사를 상대로 乙 등은 이직하면서 甲 회사의 허락 없이 
                  위 기술정보를 복사하여 유출하였고 丙 회사는 이를 사용하여 레이저 드릴링 장비 등 
                  반도체 장비를 제작·판매함으로써 甲 회사의 영업비밀 및 저작권을 침해하였다며 
                  손해배상을 구하는 본안소송을 제기한 다음, 위 손해배상청구권을 피보전권리로 
                  丙 회사의 丁 주식회사에 대한 물품대금채권의 가압류를 신청하여 가압류결정을 
                  받았으나, 본안소송에서 위 가압류 청구금액의 약 1/170에 해당하는 금액만을 
                  손해배상채권액으로 인정하는 판결이 내려져 확정되자, 丙 회사가 甲 회사를 상대로 
                  부당 가압류에 따른 손해배상을 구한 사안이다.
                </p>
                <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
              </td>
            </tr>
            <tr class="table_desc">
              <th scope="row">4</th>
              <td>
                <p>
                  확정판결의 기판력은 확정판결의 주문에 포함된 법률적 판단과 동일한 사항이 
                  소송상 문제가 되었을 때 당사자는 이에 저촉되는 주장을 할 수 없고 법원도 이에 
                  저촉되는 판단을 할 수 없는 기속력을 의미하고, 확정판결의 내용대로 실체적 
                  권리관계를 변경하는 실체법적 효력을 갖는 것은 아니다.
                </p>
                <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
              </td>
            </tr>
            <tr class="table_desc">
              <th scope="row">5</th>
              <td>
                <p>
                  발명을 한 사람 또는 그 승계인은 특허법에서 정하는 바에 따라 특허를 받을 수
                   있는 권리를 가진다(특허법 제33조 제1항 본문). 만일 이러한 정당한 권리자 
                   아닌 사람(이하 ‘무권리자’라 한다)이 한 특허출원에 대하여 특허권의 설정등록이 
                   이루어지면 특허무효사유에 해당한다(특허법 제133조 제1항 제2호). 특허출원 전에 
                   특허를 받을 수 있는 권리를 계약에 따라 이전한 양도인은 더 이상 그 권리의 
                   귀속주체가 아니므로 그러한 양도인이 한 특허출원에 대하여 설정등록이 이루어진 
                   특허권은 특허무효사유에 해당하는 무권리자의 특허이다.
                  </p>
                  <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
                </td>
            </tr>
            <tr class="table_desc">
              <th scope="row">6</th>
              <td>
                <p>
                  확정판결의 기판력은 확정판결의 주문에 포함된 법률적 판단과 동일한 사항이 
                  소송상 문제가 되었을 때 당사자는 이에 저촉되는 주장을 할 수 없고 법원도 
                  이에 저촉되는 판단을 할 수 없는 기속력을 의미하고, 확정판결의 내용대로 실체적 
                  권리관계를 변경하는 실체법적 효력을 갖는 것은 아니다.
                </p>
                <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
              </td>
            </tr>
            <tr class="table_desc">
              <th scope="row">7</th>
              <td>
                <p>
                  발명을 한 사람 또는 그 승계인은 특허법에서 정하는 바에 따라 특허를 받을 수 
                  있는 권리를 가진다(특허법 제33조 제1항 본문). 만일 이러한 정당한 권리자 아닌 
                  사람(이하 ‘무권리자’라 한다)이 한 특허출원에 대하여 특허권의 설정등록이 
                  이루어지면 특허무효사유에 해당한다(특허법 제133조 제1항 제2호). 특허출원 전에 
                  특허를 받을 수 있는 권리를 계약에 따라 이전한 양도인은 더 이상 그 권리의 
                  귀속주체가 아니므로 그러한 양도인이 한 특허출원에 대하여 설정등록이 이루어진 
                  특허권은 특허무효사유에 해당하는 무권리자의 특허이다.
                </p>
                <a class="btn" href="#ex1"><button type="button" >더보기</button></a>
              </td>
            </tr>
        </table>
        </div>
      </div>
    </div>
  </main>

  <div id="ex1" class="modal">
    <span class="modal_title">판례</span>
    <p class="modal_desc">
      甲 주식회사는 반도체 제조장비 전문 생산업체로서 반도체 소자와 기판 사이에 
      채워진 부도체를 관통하여 통로를 형성하는 레이저 드릴링 기술을 구현한 반도체 
      장비의 제조 관련 기술정보를 보유한 회사이고, 乙 등은 甲 회사에서 근무하다가 
      또 다른 반도체 제조장비 전문 생산업체인 丙 주식회사로 이직한 사람들인데, 
      甲 회사가 乙 등 및 丙 회사를 상대로 乙 등은 이직하면서 甲 회사의 허락 없이 
      위 기술정보를 복사하여 유출하였고 丙 회사는 이를 사용하여 레이저 드릴링 장비 등 
      반도체 장비를 제작·판매함으로써 甲 회사의 영업비밀 및 저작권을 침해하였다며 
      손해배상을 구하는 본안소송을 제기한 다음, 위 손해배상청구권을 피보전권리로 
      丙 회사의 丁 주식회사에 대한 물품대금채권의 가압류를 신청하여 가압류결정을 
      받았으나, 본안소송에서 위 가압류 청구금액의 약 1/170에 해당하는 금액만을 
      손해배상채권액으로 인정하는 판결이 내려져 확정되자, 丙 회사가 甲 회사를 상대로 
      부당 가압류에 따른 손해배상을 구한 사안이다.
    </p>
  </div>

  <%@ include file="../includes/footer.jsp" %>


  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js"
    integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <script type="text/javascript" src="js/chat.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

  <script>
    $('a[href="#ex1"]').click(function(event) {
      event.preventDefault();
 
      $(this).modal({
        fadeDuration: 250
      });
    });
</script>


  
</body>

</html>