function Message(arg) {
    this.text = arg.text;
    this.message_side = arg.message_side;
    this.chatTime = arg.chatTime;
    this.draw = function (_this) {
        return function () {
            let $message;
            $message = $($('.message_template').clone().html());
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $message.find('.chatting_time').html(_this.chatTime)
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
}

//input value 받아오기
function getMessageText() {
    let $message_input;
    $message_input = $('.message_input');
    return $message_input.val();
}
yoilList = ["일", "월", "화", "수", "목", "금", "토"];
//time
function formatAMPM(date) {
    let year = date.getFullYear(); // 년도
    let month = date.getMonth() + 1;  // 월
    let day = date.getDate();  // 날짜
    let yoil = date.getDay();  // 요일
    let textToday = `${year}-${month}-${day}-${yoilList[yoil]} `
    var hours = date.getHours();
    var minutes = date.getMinutes();
    let seconds = date.getSeconds();  // 초
    let milliseconds = date.getMilliseconds(); // 밀리초
    var ampm = hours >= 12 ? 'PM' : 'AM';
    let koAmpm = hours >= 12 ? '오후' : '오전';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    let detailTime = `${textToday} ${koAmpm} ${hours}시 ${minutes}분 ${seconds}.${milliseconds}초`;
    return [strTime, detailTime];
}




//채팅 띄우기
function sendMessage(text, message_side) {
    let date = formatAMPM(new Date());
    saveChatLog(text, message_side, date[1]);
    let $messages, message;
    $('.message_input').val('');
    $messages = $('.messages');
    message = new Message({
        text: text,
        message_side: message_side,
        chatTime: date[0]
    });
    message.draw();
    $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
}



//처음 인사말
function greet() {
    setTimeout(function () {
        sendMessage(
            `안녕하세요 저는 노무관련 상담을 해주는 <br>노무봇 입니다
            <br>노무관련 질문을 입력해주시거나 <br>하단의 카테고리 버튼을 선택해주세요<br>
            <div class=cate>
                <button class=cate_btn onclick=intentCategory('노무일반')>노무일반</button>
                <button class=cate_btn onclick=intentCategory('보험')>4대보험</button>
                <button class=cate_btn onclick=intentCategory('퇴직연금')>퇴직연금</button>
                <button class=cate_btn onclick=intentCategory('급여세금')>급여세금</button>
                <button class=cate_btn onclick=intentCategory('임금일반')>임금일반</button>
                <button class=cate_btn onclick=intentCategory('근로계약')>근로계약</button>
                <button class=cate_btn onclick=intentCategory('근로조건')>근로조건</button>
                <button class=cate_btn onclick=intentCategory('산업안전')>산업안전</button>
            </div>`
            , 'left');
    }, 1000);
}
// 챗봇 엔터 가능 
function onClickAsEnter(e) {
    if (e.keyCode === 13) {
        onSendButtonClicked()
    }
}


//연관 키워드 변경
function requestRelationWord(middle, main) {
    $('.relation p').empty();
    if (Array.isArray(main)) {
        for (let i in main) {
            $('.relation p').append(`<a href='#'><span>${main[i]}</span></a>`)
        }
    } else {
        $('.relation p').append(`<a href='#'><span>${main}</span></a>`)
    }
    if (middle !== 'no') {
        $('.relation p').append(`<a href='#'><span>${middle}</span></a>`)
    }
}



// 노무사전 키워드 변경
function requestDictionary(nomu_dict) {
    $('.dictionary p').empty();
    for (let i in nomu_dict) {
        if(nomu_dict[i] === null){
            $('.dictionary p').append(`<a href='#'><span>사전에 없습니다</span></a>`)
        } 
        else{
            $('.dictionary p').append(`<a href='#'><span>${nomu_dict[i]}</span></a>`)
        }
        
    }
    
}
//추천 키워드 변경
function requestRecommend(word) {
    $('.recommend_keyword p').empty();
    for (let i in word) {
        $('.recommend_keyword p').append(`<a href='#'><span>${word[i]}</span></a>`)
    }
}

//키워드 값 변경을 위해서 데어터 가져옴
function requestChat(messageText, url_pattern) {
    let msg = encodeURIComponent(messageText)
    $.ajax({
        url: "http://192.168.0.11:5000/" + url_pattern + "/" + msg,
        type: "GET",
        dataType: "json",
        error: function (request, status, error) {
            return sendMessage('죄송합니다. 무슨말인지 잘 모르겠어요.', 'left');
        }
    }).done(function (data) {

        if (url_pattern === 'request_chat') {
            if (data.hasOwnProperty('leadingcase')) {
                requestLeadingCase(data['leadingcase']);
            }
            if (data.hasOwnProperty('case')) {
                requestCase(data['case']);
            }
            if (data['middleCategory'] != null) {
                requestRelationWord(data['middleCategory'], data['mainCategory']);
            } else if (data['mainCategory'] != null) {
                requestRelationWord('no', data['mainCategory']);
            }
            if (data['recommend'] != null) {
                requestRecommend(data['recommend']);
            }
            if (data['nomu_dict'] != null){
                requestDictionary(data['nomu_dict']);
            }
            if (data['wordcloud'] != null){
                requestWordcloud(data['wordcloud']);
            }
            for (let i in data['answer']) {
                (function (x) {
                    setTimeout(function () {
                        sendMessage(data['answer'][x], 'left');
                    }, 1000 * x);
                })(i);
            }
        } else {
            return sendMessage('죄송합니다. 무슨말인지 잘 모르겠어요.', 'left');
        }
    })

}

// 챗봇 로그 저장
function saveChatLog(messageText, side, time) {
    let msg = encodeURIComponent(messageText)
    $.ajax({
        url: "http://192.168.0.11:5000/save_log/" + msg + "/" + side + "/" + time,
        type: "GET",
        dataType: "json",

    }).done(function (data) {

    });
}
// 판례와 사례 변경
function requestLeadingCase(leadingcase) {
    leadingcaseContent = "";
    if (leadingcase == null) {
        leadingcaseContent = "<p>아직 판례가 없습니다.</p>";
    } else {
        leadingcaseContent = leadingcase
    }
    $('.judicial p').remove()
    $('.judicial').append(`<p>${leadingcaseContent}</p>`)
}

function requestCase(exampleCase) {
    caseContent = "";
    if (exampleCase == null) {
        caseContent = "<p>아직 사례가 없습니다.</p>";
    } else {
        caseContent = exampleCase
    }
    $('.case p').remove()
    $('.case').append(`<p>${exampleCase}</p>`)
}


// 챗봇 특정 단어 입력시 반응
function onSendButtonClicked() {
    let messageText = getMessageText();
    if (messageText.trim() === ""){
        alert("질문을 입력해 주세요.")
    }
    else if (messageText.includes('/')) {
        setTimeout(function () {
            return sendMessage("죄송합니다. 무슨말인지 잘 모르겠어요.", 'left');
        }, 1000);
    }else if (messageText.includes('안녕')) {
        setTimeout(function () {
            return sendMessage("안녕하세요. 저는 노무상담 채팅봇 입니다.", 'left');
        }, 1000);
    } else if (messageText.includes('고마워')) {
        setTimeout(function () {
            return sendMessage("천만에요. 더 물어보실 건 없나요?", 'left');
        }, 1000);
    } else if (messageText.includes('없어')) {
        setTimeout(function () {
            return sendMessage("그렇군요. 알겠습니다!", 'left');
        }, 1000);
    }else if(messageText === ('처음')){
      greet();
    }else if(messageText == ('지우기')){
      $('.messages').empty();
      greet();
   }else {
    sendMessage(messageText, 'right');
    return requestChat(messageText, 'request_chat');
    }
}

greet();


// 각 태그 별 질문 2개씩
let btnQuestion = {
    '임금일반1': '고정연장수당 및 식대도 통상임금으로 보아야 하나요?',
    '임금일반2': '만근수당은 통상임금에 포함되나요?',
    '급여세금1': '특정월 중도 퇴사자의 경우 급여 계산 방법은?',
    '급여세금2': '월급제 직원의 시급 계산은 어떻게 하는지요?',
    '보험1': '보수월액 변경신고, 반드시 해야 하나요?',
    '보험2': '중도입사자에 대한 건강보험료 정산이 전부 환급이 나온 이유가 있나요?',
    '퇴직연금1': '1년 미만 근로자가 관계사 전출 시 퇴직금은 어떻게 처리해야 하나요?',
    '퇴직연금2': '근로시간 단축으로 퇴직금이 감소되면 중간정산이 가능한가요?',
    '노무일반1' : '외국인 근로자 계약기간 연장을 어떻게 해야 할까요?',
    '노무일반2' : '출국만기보험 퇴직금 원천징수 주체는 누군가요?',
    '근로계약1' : '직장 다니면서 유투브 하면 투잡으로 문제가 되나요?',
    '근로계약2' : '3개월 수습 시 근로계약서는 어떻게 작성해야 하나요?',
    '근로조건1' : '주 5일 주 35시간 사업장에서 1일 7시간 초과근무 시 적용 기준은 어떻게 되나요?',
    '근로조건2' : '2조 2교대를 운용 중인데 법 위반이 되나요?',
    '산업안전1' : '1년 미만의 근로자가 산업재해를 입었는데 퇴직금이 발생되나요?',
    '산업안전2' : '미신고 산업재해를 고용노동부에서 알 수 있을까요?'
}
// 태그 별 질문에 대한 답변
function intentCategory(intent) {
    if (intent === '임금일반') {
        sendMessage(`
            임금 관련 질문입니다.<br>
            1. ${btnQuestion['임금일반1']}
            <button onclick=answerBtnQuestion('임금일반1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['임금일반2']}
            <button onclick=answerBtnQuestion('임금일반2')>답변</button>
            `, 'left');
    } else if (intent === '보험') {
        sendMessage(`
            4대보험 관련 질문입니다.<br>
            1. ${btnQuestion['보험1']}
            <button onclick=answerBtnQuestion('보험1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['보험2']}
            <button onclick=answerBtnQuestion('보험2')>답변</button>
            `, 'left');
    } else if (intent === '급여세금') {

        sendMessage(`
            금여세금 관련 질문입니다.<br>
            1. ${btnQuestion['급여세금1']}
            <button onclick=answerBtnQuestion('급여세금1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['급여세금2']}
            <button onclick=answerBtnQuestion('급여세금2')>답변</button>
            `, 'left');
    } else if (intent === '퇴직연금') {
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['퇴직연금1']}
        <button onclick=answerBtnQuestion('퇴직연금1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['퇴직연금2']}
        <button onclick=answerBtnQuestion('퇴직연금2')>답변</button>
        `, 'left');
    } else if (intent === '노무일반') {
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['노무일반1']}
        <button onclick=answerBtnQuestion('노무일반1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['노무일반2']}
        <button onclick=answerBtnQuestion('노무일반2')>답변</button>
        `, 'left');
    } else if (intent === '근로계약') {
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['근로계약1']}
        <button onclick=answerBtnQuestion('근로계약1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['근로계약2']}
        <button onclick=answerBtnQuestion('근로계약2')>답변</button>
        `, 'left');
    } else if (intent === '근로조건') {
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['근로조건1']}
        <button onclick=answerBtnQuestion('근로조건1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['근로조건2']}
        <button onclick=answerBtnQuestion('근로조건2')>답변</button>
        `, 'left');
    } else if (intent === '산업안전') {
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['산업안전1']}
        <button onclick=answerBtnQuestion('산업안전1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['산업안전2']}
        <button onclick=answerBtnQuestion('산업안전2')>답변</button>
        `, 'left');
    }
}

function requestWordcloud(wordcloud){
    $('.cloud').empty();
    if(wordcloud != null){
        $('.cloud').append('<img src=../../images/wordcloud/'+ wordcloud + '></img>')
    }
    
}


// 챗봇 상단 홈으로 가는 버튼
$('.btn_prev').click(function () {
    greet();
});
// 챗봇 상단 지우개 버튼
$('.btn_clear').click(function () {
    $('.messages li').remove();
});
// 태그 별 질문지 답변 버튼 기능
function answerBtnQuestion(question) {
    sendMessage(btnQuestion[question], 'right');
    setTimeout(function () {
        requestChat(btnQuestion[question], 'request_chat');
    }, 1000);
}