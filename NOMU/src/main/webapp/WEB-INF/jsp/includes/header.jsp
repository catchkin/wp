<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


	
	<!-- 자바 프로그램을 태그로 사용하도록 정의-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--숫자나 통화, 날짜 같은 형태 맞춰주기 위하여 정의-->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import ="egovframework.com.cmm.LoginVO" %>
<!DOCTYPE html>
<html lang="ko">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>NOMU</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
  <!--CDN 링크 -->
  <!-- <link rel="stylesheet" href="../../css/dictionary.css"> -->
  <%-- <link rel="stylesheet" href="<c:url value='/'/>css/portal_css/default.css" type="text/css" > --%>
  <link rel="stylesheet" href="<c:url value='/'/>css/dictionary.css" type="text/css" >
  <!-- <link rel="stylesheet" href="../../css/style.css"> -->
  <!-- <link rel="stylesheet" href="resources/css/chat.css"> -->
  
  <!-- js -->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="../../js/chat.js"></script>
</head>

<body>
  <header>
    <div class="container">
      <a href="/NOMU"><h1>NOMU</h1></a>
    </div>
  </header>

  <nav>
    <div class="container">
      <ul class="main_nav">
        <li class="nomu_info"><a href='<c:url value="/leadingcase.do"/>'>마이페이지</a>
          <ul class="sub_nav sub1">
            <li><a href='<c:url value="/leadingcase.do"/>'>내정보</a></li>
            <li><a href='<c:url value="/example.do"/>'>대화이력</a></li>
            <li><a href='<c:url value="/news.do"/>'>관리정보</a></li>
          </ul>
        </li>
        <li><a href='<c:url value="/leadingcase.do"/>'>노무정보</a>
        <ul class="sub_nav sub1">
            <li><a href='<c:url value="/leadingcase.do"/>'>판례</a></li>
            <li><a href='<c:url value="/example.do"/>'>사례</a></li>
            <li><a href='<c:url value="/news.do"/>'>뉴스</a></li>
            <li><a href='<c:url value="/dictionary.do"/>'>노무사전</a></li>
          </ul>
        </li>
        <li class="service"><a href='<c:url value="/labor.do"/>'>부가서비스</a>
          <ul class="sub_nav sub2">
            <li><a href='<c:url value="/labor.do"/>'>노무사정보</a></li>
            <li><a href="/uss/umt/mber/EgovMberManage.do">일자리정보</a></li>
            <li><a href="#">교육정보</a></li>
          </ul>
        </li>
        <li><a href="<c:url value='/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_BBBBBBBBBBBB'/>">게시판</a></li>
        <li><a href="<c:url value='/uss/olh/faq/FaqListInqire.do'/>">FAQ</a></li>
    </div>
  </nav>