<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>

  <main class="container">
      <div class="dic1">
          <div class="inputarea">
            <input type="text" placeholder="검색어를 입력해주세요">
            <button type="button"><img src="images/nomu/search_icon.png" alt="search_icon"></button>
          </div>
      </div>
      <div class="dic2">
        <div class="dic_category">
            <table class="dic_table">
                <tr>
                    <td>
                        <strong>개인연금</strong>
                        <p>
                            생명 보험 회사나 신탁 은행이 개인에게 상품으로 판매하는 연금 지불형의 보험이나 신탁,
                            개인이 보험 회사나 은행에 일정액을 불입한 후 일정한 나이가 되면
                            해마다 일정액을 지급받는다
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>계약일</strong>
                        <p>
                            건설에서, 시공주와의 계약이 체결된 일자, 자기 공사인 경우에는 착공일자를 계약일자로 본다.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>실업급여</strong>
                        <p>
                            경제정부, 고용인, 피고용자가 공동으로 비용을 분담하는 사회 보험 제도의 하나, 근로자가
                            실직할 경우 일정 금액 지급을 받는것으로, 1996년 7월부터 실시되었다. 구직급여, 상병급여,
                            취직촉진수당, 연장급여로 구분된다
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>연금</strong>
                        <p>
                            국가나 사회에 특별한 공로가 있거나 일정기간동안 국가기관에 복무한 사람에게 해마다 주는 돈, 무상연금
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>고용보험</strong>
                        <p>
                            근로자가 실직한 경우에 생활안정을 위하여 일정기간 동안 급여를 지급하는 실업급여사업과 함께 구직자에 대한 직업능력개발·향상 및 적극적인 취업알선을 통한 재취업의 촉진과 실업예방을 위하여 고용안정사업 및 직업능력개발사업 등의 실시를 목적으로 하는 사회보험의 하나
                    </td>
                </tr>
                
            </table>
            <div class="paging">
              <ul>
                <li><a href="#"><img src="images/nomu/leftArrow.png" alt="leftArrow"></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><img src="images/nomu/rightArrow.png" alt="rightArrow"></a></li>
            </ul>
            </div>
        </div>
      </div>
  </main>
  <%@ include file="../includes/footer.jsp" %>


  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.min.js"></script>
  <script type="text/javascript" src="js/paging.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.min.js"></script>
    

</body>

</html>