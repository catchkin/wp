<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    


	<main class="container">
    <div class="flex">
      <div class="content1">
        <div class="add_service">
          <div class="add_box">
            <h1>부가서비스</h1>
            <div class="labor_info">
              <h2>노무사 정보</h2>
              <div class="profile">
                <img src="./images/woman.jpg" alt="프로필">
                <div class="info">
                  <ul>
                    <li><a href="#" class="name">홍길동</a>/<span class="type">임금체불전문</span></li>
                    <li><a href="#" class="desc">임금/퇴직금</a><span class="time">(10분)</span></li>
                    <li><a href="#" class="price">10,000원</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="job_info">
              <h2>일자리 정보</h2>

            </div>
            <div class="edu_info">
              <h2>교육 정보</h2>
            </div>
          </div>
        </div>
        <div class="note">
          <div class="note_box">
            <div class="notice">
              <div class="note_head">
                <h2>게시판</h2>
                <a href="./board.html"><button type="button">더보기 +</button></a>
              </div>
              <ul>
                <li><a href="#">실업급여 관련 질문해봅니다</a></li>
                <li><a href="#">이거 정당한 해고 맞나요?</a></li>
                <li><a href="#">억울한 일 당했던 썰 푼다</a></li>
              </ul>
            </div>
            <div class="faq">
              <div class="note_head">
                <h2>FAQ</h2>
                <a href="./faq.html"><button type="button">더보기 +</button></a>
              </div>
              <ul>
                <li><a href="#">내가 원하는 자료가 뜨지 않는다</a></li>
                <li><a href="#">노무사 상담을 받으려면 나를 만나라</a></li>
                <li><a href="#">챗봇 사용방법에 대해서</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="content2">
        <!--챗봇-->
        <div class="chat_window">
          <div class="top_menu">
            <div class="chat_btn">
              <div class="left">
                <button type="button"><img src="./images/chat_question.png" alt="clear"></button>
              </div>
              <div class="right">
                <button type="button"><img src="./images/chat_prev.png" alt="clear"></button>
                <button type="button"><img src="./images/chat_clear.png" alt="clear"></button>
              </div>
            </div>
          </div>
          <ul class="messages"></ul>
          <div class="category">
          </div>
          <div class="bottom_wrapper">
            <div class="input_box">
              <input class="message_input" onkeyup="return onClickAsEnter(event)" placeholder="질문을 입력해주세요." />
              <div class="send_message" id="send_message" onclick="onSendButtonClicked()">
  
                <div class="icon"></div>
                <div class="text"><img src="./images/submit.png" alt="submit"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="message_template">
          <li class="message">
            <div class="chat_text_time">
              <div class="avatar"></div>
              <div class="text_wrapper">
                <div class="text"></div>
              </div>
            </div>
          </li>
        </div>
        <!--챗봇-->
      </div>
      
      <div class="content3">
        <div class="con_box">
          <div class="judicial">
            <div class="news_head">
              <h2>판례</h2>
              <a href="./leadingcase.html"><button type="button">더보기 +</button></a>
            </div>
            <p>
              우위위이이잉. 오전 7시 정각이 되자 거대한
              소음과 함께 육중한 기계가 구르기 시작했다.
              몇 분쯤 지났을까. 공회전하던 50m 길이의
              철제 레일 위로 크고 작은 택배 상자가 하나둘
              실려나왔다. 레일 앞에 선 심복선씨(42)의
              눈과 손이 분주해졌다 공회전하던 50m 길이의
              철제 레일 위로 크고 작은 택배 상자가 하나둘
              실려나왔다. 레일 앞에 선 심복선씨(42)의
              눈과 손이 분주해졌다
            </p>
          </div>
          <div class="case">
            <div class="news_head">
              <h2>사례</h2>
              <a href="./example.html"><button type="button">더보기 +</button></a>
            </div>
            <p>
              최근 유난히 업무량이 많이 늘어난 곳이 있다면
              바로 비대면서비스를 해야 하는 곳들과 배달이나
              택배업을 하는 직종이라고 합니다. 택배 폭증에
              기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
              택배 폭증에 기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
            </p>
          </div>
          <div class="news">
            <div class="news_head">
              <h2>뉴스</h2>
              <a href="./news.html"><button type="button">더보기 +</button></a>
            </div>
            <strong>택배기사 뇌출혈산재 승인 사례</strong>
            <p>
              최근 유난히 업무량이 많이 늘어난 곳이 있다면
              바로 비대면 서비스를 해야 하는 곳들과 배달
              이나 택배업을 하는 직종이라고 합니다.
              택배 폭증에 기사분들의 과로 정도가 크다고하며
              신규 기사에게는 이에 따라 6-70%만 업무를
              배정하도록 하고 택배차량과 기사를 신속하 처리한다
              택배 폭증에 기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
            </p>
          </div>
        </div>
      </div>
      <div class="content4">
        <div class="login">
          <div class="login_box">
            <h2>로그인</h2>
            <form name="login" action="#" method="GET">
              <fieldset>
                <input id="id" name="id" type="text" placeholder="아이디를 입력해주세요" required>
                <input id="pwd" name="pwd" type="password" placeholder="비밀번호를 입력해주세요" required>
              </fieldset>
              <a href="signup.html" class="register">회원가입</a>
              <button type="submit">로그인</button>
            </form>

          </div>
        </div>
        <div class="cloud">

        </div>
        <div class="mycontent">
          <div class="my_box">
            <div class="dictionary">
              <div class="dict_box">
                <h2>노무사전</h2>
                <a href="./dictionary.html"><button type="button">더보기 +</button></a>
              </div>
              <p>
                <a href="#">#<span>실업급여</span></a>
              </p>
            </div>
            <div class="recommend_keyword">
                <div class="dict_box">
                  <h2>추천키워드</h2>
                  <a href="#"><button type="button">더보기 +</button></a>
                </div>
                <p>
                  <a href="#">#<span>코로나19</span></a>
                </p>
              </div>
            <div class="relation">
              <div class="dict_box">
                <h2>연관키워드</h2>
                <a href="#"><button type="button">더보기 +</button></a>
              </div>
              <p>
                <a href="#">#<span>고용노동부</span></a>
                <a href="#">#<span>산재보험</span></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </main>


<!--   <script src="https://code.jquery.com/jquery-1.12.4.min.js"
    integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="./js/media.js"></script>
  <script type="text/javascript" src="./js/chat.js"></script> -->
  
 </body>
</html>
