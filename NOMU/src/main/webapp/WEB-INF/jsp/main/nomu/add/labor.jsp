<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../../includes/header.jsp"%>

<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<main class="container">
<div class="flex">
	<div class="info_box">
	<div class="content5">
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">이기준</a>/<span class="type">산업재해, 노동조합 설립 및 운영</span></li>
								<li><a href="#" class="desc">산업재해 및 노조활동<br>명쾌한 상담제공</a><span class="time">(20분)</span></li>
								<li><a href="#" class="price">10,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">민승기</a>/<span class="type">근로계약, 4대보험</span></li>
								<li><a href="#" class="desc">임금체불,부당해고 등 노동문제 상담</a><br><span class="time">최대 작업 소요 1일</span></li>
								<li><a href="#" class="price">10,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
		</div>
			<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">오유림</a>/<span class="type">근로계약,취업규칙</span></li>
								<li><a href="#" class="desc">나홀로 체당금 신청하기</a><span class="time">(20분)</span></li>
								<li><a href="#" class="price">30,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="content5">
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">박 준</a>/<span class="type">근로계약,취업규칙</span></li>
								<li><a href="#" class="desc">해고,임금체불 등</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">10,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">원혜경</a>/<span class="type">임금체불,근로시간,휴가,부당해고</span></li>
								<li><a href="#" class="desc">연차,퇴직금 관련 상담</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">20,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">진휘민</a>/<span class="type">채당금,근로계약,취업규칙</span></li>
								<li><a href="#" class="desc">근로자와 사업주를 위한 노무상담</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">10,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="content5">
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">이상영</a>/<span class="type">임금체불,부당해고</span></li>
								<li><a href="#" class="desc">임금체불 진정/부당해고 전문 상담</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">20,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">원혜경</a>/<span class="type">임금체불,근로시간,휴가,부당해고</span></li>
								<li><a href="#" class="desc">연차,퇴직금 관련 상담</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">20,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<div class="add_service">
			<div class="add_box">
				<div class="labor_info">
					<h2>노무사 정보</h2>
						<div class="info">
							<ul>
								<li><a href="#" class="name">진휘민</a>/<span class="type">채당금,근로계약,취업규칙</span></li>
								<li><a href="#" class="desc">근로자와 사업주를 위한 노무상담</a><span class="time">(10분)</span></li>
								<li><a href="#" class="price">10,000원</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="../../../includes/footer.jsp"%>

</main>
<script>
function fn_egov_select_linkPage(pageNo){
	
	document.FaqListForm.pageIndex.value = pageNo;
	document.FaqListForm.action = "<c:url value='/labor.do'/>";
   	document.FaqListForm.submit();
   	
}
</script>
</body>
</html>
